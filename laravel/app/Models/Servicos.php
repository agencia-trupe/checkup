<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Helpers\CropImage;

class Servicos extends Model
{
    protected $table = 'servicos';

    protected $guarded = ['id'];

}
