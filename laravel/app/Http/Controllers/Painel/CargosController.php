<?php

namespace App\Http\Controllers\Painel;

use Illuminate\Http\Request;

use App\Http\Requests\CargosRequest;
use App\Http\Controllers\Controller;

use App\Models\Cargo;

class CargosController extends Controller
{
    public function index()
    {
        $registros = Cargo::ordenados()->get();

        return view('painel.cargos.index', compact('registros'));
    }

    public function create()
    {
        return view('painel.cargos.create');
    }

    public function store(CargosRequest $request)
    {
        try {

            $input = $request->all();

            Cargo::create($input);

            return redirect()->route('painel.cargos.index')->with('success', 'Registro adicionado com sucesso.');

        } catch (\Exception $e) {

            return back()->withErrors(['Erro ao adicionar registro: '.$e->getMessage()]);

        }
    }

    public function edit(Cargo $registro)
    {
        return view('painel.cargos.edit', compact('registro'));
    }

    public function update(CargosRequest $request, Cargo $registro)
    {
        try {

            $input = $request->all();

            $registro->update($input);

            return redirect()->route('painel.cargos.index')->with('success', 'Registro alterado com sucesso.');

        } catch (\Exception $e) {

            return back()->withErrors(['Erro ao alterar registro: '.$e->getMessage()]);

        }
    }

    public function destroy(Cargo $registro)
    {
        try {

            $registro->delete();

            return redirect()->route('painel.cargos.index')->with('success', 'Registro excluído com sucesso.');

        } catch (\Exception $e) {

            return back()->withErrors(['Erro ao excluir registro: '.$e->getMessage()]);

        }
    }

}
