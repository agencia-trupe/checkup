<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests\ContatosRecebidosRequest;
use App\Http\Controllers\Controller;

use App\Models\Banner;
use App\Models\Empresa;
use App\Models\Servicos;
use App\Models\Cargo;
use App\Models\ContatoRecebido;
use App\Models\Contato;

class HomeController extends Controller
{
    public function index()
    {
        $banners = Banner::ordenados()->get();

        return view('frontend.home', compact('banners'));
    }

    public function empresa()
    {
        $empresa = Empresa::first();

        return view('frontend.empresa', compact('empresa'));
    }

    public function servicos()
    {
        $servicos = Servicos::first();

        return view('frontend.servicos', compact('servicos'));
    }

    public function proposta()
    {
        $cargos = Cargo::ordenados()->lists('titulo', 'titulo');

        return view('frontend.proposta', compact('cargos'));
    }

    public function propostaPost(ContatosRecebidosRequest $request, ContatoRecebido $contatoRecebido)
    {
        $input = $request->all();

        $contatoRecebido->create($input);

        $contato = Contato::first();

        if (isset($contato->email)) {
            \Mail::send('emails.contato', $input, function($message) use ($request, $contato)
            {
                $message->to($contato->email, config('site.name'))
                        ->subject('[CONTATO] '.config('site.name'))
                        ->replyTo($request->get('email'), $request->get('nome'));
            });
        }

        return back()->with('success', true);
    }

    public function contato()
    {
        return view('frontend.contato');
    }
}
