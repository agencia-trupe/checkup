<?php

namespace App\Http\Requests;

use App\Http\Requests\Request;

class ServicosRequest extends Request
{
    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        return [
            'servicos' => 'required',
            'terceirizacao_beneficios' => 'required',
        ];
    }
}
