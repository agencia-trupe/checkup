<?php

namespace App\Http\Requests;

use App\Http\Requests\Request;

class ContatosRecebidosRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'nome_condominio'         => 'required',
            'endereco'                => 'required',
            'apartamentos'            => 'required',
            'funcionarios'            => 'required',
            'valor_mensal_condominio' => 'required',
            'nome'                    => 'required',
            'email'                   => 'required|email',
            'telefone'                => 'required',
            'cargo'                   => 'required',
            'mensagem'                => 'required'
        ];
    }
}
