@extends('frontend.common.template')

@section('content')

    <div class="main center">
        <div class="contato">
            <p>{!! $contato->texto !!}</p>
            <img src="{{ asset('assets/img/layout/checkup-marca2.png') }}" alt="">
            <p>{!! $contato->endereco !!}</p>
        </div>
    </div>

@endsection
