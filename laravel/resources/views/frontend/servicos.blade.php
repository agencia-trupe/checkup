@extends('frontend.common.template')

@section('content')

    <div class="main center">
        <div class="servicos">
            <h2>SERVIÇOS</h2>
            {!! $servicos->servicos !!}

            <div class="grafico-wrapper">
                <div class="grafico">
                    <div class="item-1">PORTARIA</div>
                    <div class="item-2">
                        <span>SERVIÇOS GERAIS</span>
                        <span>
                            LIMPEZA<br>
                            MANUTENÇÃO<br>
                            JARDINAGEM
                        </span>
                    </div>
                    <div class="setas"></div>
                    <div class="quadro-2">
                        <img src="{{ asset('assets/img/layout/checkup-marca2.png') }}" alt="">
                    </div>
                    <div class="quadro-2-texto">
                        <span>TERCEIRIZAÇÃO</span>
                        <span>REDUÇÃO<br>DE CUSTOS</span>
                    </div>
                    <div class="quadro-3">PROFISSIONAIS ALTAMENTE QUALIFICADOS</div>
                    <div class="quadro-4">
                        <span>SELEÇÃO ATRAVÉS DE RIGOROSOS TESTES DE APTIDÃO</span>
                        <span>TREINAMENTO E RECICLAGEM CONTÍNUOS</span>
                        <span>CHECAGEM DE REFERÊNCIAS TRABALHISTAS</span>
                        <span>CHECAGEM DE ANTECEDENTES CRIMINAIS</span>
                        <span>PLANO DE BENEFÍCIOS ESTIMULADOR</span>
                    </div>
                </div>
            </div>

            <h2>TERCEIRIZAÇÃO - BENEFÍCIOS</h2>
            {!! $servicos->terceirizacao_beneficios !!}
        </div>
    </div>

@endsection
