    <header>
        <div class="center">
            <a href="{{ route('home') }}" class="logo">{{ $config->nome_do_site }}</a>
            <nav id="nav-desktop">
                @include('frontend.common.nav')
                <button id="mobile-toggle" type="button" role="button">
                    <span class="lines"></span>
                </button>
                <div class="contato">
                    @if($contato->facebook)
                    <a href="{{ $contato->facebook }}" target="_blank">facebook</a>
                    @endif
                    <span>FONE: {{ $contato->fone }}</span>
                </div>
            </nav>
            <nav id="nav-mobile">
                @include('frontend.common.nav')
            </nav>
        </div>
    </header>
