<a href="{{ route('empresa') }}" @if(Tools::isActive('empresa')) class="active" @endif>A CHECKUP</a>
<a href="{{ route('servicos') }}" @if(Tools::isActive('servicos')) class="active" @endif>SERVIÇOS</a>
<a href="{{ route('proposta') }}" @if(Tools::isActive('proposta')) class="active" @endif>SOLICITE UMA PROPOSTA</a>
<a href="{{ route('contato') }}" @if(Tools::isActive('contato')) class="active" @endif>CONTATO</a>
<a href="http://www.gaboardiegomes.com.br/" target="_blank">GABOARDI & GOMES</a>
