@extends('frontend.common.template')

@section('content')

    <div class="main center">
        <div class="empresa">
            <div class="texto">{!! $empresa->texto !!}</div>
            <div class="box">
                <h3>MISSÃO</h3>
                <div>{!! $empresa->missao !!}</div>
                <h3>VISÃO</h3>
                <div>{!! $empresa->visao !!}</div>
                <h3>VALORES</h3>
                <div>{!! $empresa->valores !!}</div>
            </div>
        </div>
    </div>

@endsection
