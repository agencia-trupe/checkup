@extends('frontend.common.template')

@section('content')

    <div class="main center">
        <div class="proposta">
            @if(session('success'))
            <div class="enviado">PROPOSTA ENVIADA COM SUCESSO.</div>
            @else
            <form action="{{ route('proposta.post') }}" method="POST">
                {!! csrf_field() !!}

                <p>POR GENTILEZA, PREENCHA O CADASTRO ABAIXO PARA REQUISITAR UMA PROPOSTA.</p>

                @if($errors->any())
                    <div class="erro">Preencha todos os campos corretamente.</div>
                @endif

                <div class="box">
                    <div class="col">
                        <h3>DADOS DO CONDOMÍNIO</h3>
                        <div class="row">
                            <label for="nome_condominio">Nome do condomínio</label>
                            {!! Form::text('nome_condominio', null, ['required' => true]) !!}
                        </div>
                        <div class="row">
                            <label for="endereco">Endereço</label>
                            {!! Form::text('endereco', null, ['required' => true]) !!}
                        </div>
                        <div class="row">
                            <label for="apartamentos">Quantidade de apartamentos</label>
                            {!! Form::text('apartamentos', null, ['required' => true]) !!}
                        </div>
                        <div class="row">
                            <label for="funcionarios">Funcionários</label>
                            {!! Form::text('funcionarios', null, ['required' => true]) !!}
                        </div>
                        <div class="row">
                            <label for="valor_mensal_condominio">Valor mensal do condomínio atual</label>
                            {!! Form::text('valor_mensal_condominio', null, ['required' => true]) !!}
                        </div>
                    </div>
                    <div class="col">
                        <h3>DADOS PESSOAIS</h3>
                        <div class="row">
                            <label for="nome">Nome</label>
                            {!! Form::text('nome', null, ['required' => true]) !!}
                        </div>
                        <div class="row">
                            <label for="email">E-mail</label>
                            {!! Form::email('email', null, ['required' => true]) !!}
                        </div>
                        <div class="row">
                            <label for="telefone">Telefone</label>
                            {!! Form::text('telefone', null, ['required' => true]) !!}
                        </div>
                        <div class="row">
                            <label for="cargo">Cargo</label>
                            {!! Form::select('cargo', $cargos, null, ['required' => true, 'placeholder' => 'Selecione']) !!}
                        </div>
                        <div class="row">
                            <label for="mensagem">Mensagem</label>
                            <textarea name="mensagem"></textarea>
                        </div>
                    </div>
                </div>
                <input type="submit" value="ENVIAR DADOS">
            </form>
            @endif
        </div>
    </div>

@endsection
