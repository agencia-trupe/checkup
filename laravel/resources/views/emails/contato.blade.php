<!DOCTYPE html>
<html>
<head>
    <title>[CONTATO] {{ $config->nome_do_site }}</title>
    <meta charset="utf-8">
</head>
<body>
    <span style='font-weight:bold;font-size:16px;font-family:Verdana;'>Nome do Condomínio:</span> <span style='color:#000;font-size:14px;font-family:Verdana;'>{{ $nome_condominio }}</span><br>
    <span style='font-weight:bold;font-size:16px;font-family:Verdana;'>Endereço:</span> <span style='color:#000;font-size:14px;font-family:Verdana;'>{{ $endereco }}</span><br>
    <span style='font-weight:bold;font-size:16px;font-family:Verdana;'>Quantidade de Apartamentos:</span> <span style='color:#000;font-size:14px;font-family:Verdana;'>{{ $apartamentos }}</span><br>
    <span style='font-weight:bold;font-size:16px;font-family:Verdana;'>Funcionários:</span> <span style='color:#000;font-size:14px;font-family:Verdana;'>{{ $funcionarios }}</span><br>
    <span style='font-weight:bold;font-size:16px;font-family:Verdana;'>Valor mensal do condomínio atual:</span> <span style='color:#000;font-size:14px;font-family:Verdana;'>{{ $valor_mensal_condominio }}</span><br>
    <hr>
    <span style='font-weight:bold;font-size:16px;font-family:Verdana;'>Nome:</span> <span style='color:#000;font-size:14px;font-family:Verdana;'>{{ $nome }}</span><br>
    <span style='font-weight:bold;font-size:16px;font-family:Verdana;'>E-mail:</span> <span style='color:#000;font-size:14px;font-family:Verdana;'>{{ $email }}</span><br>
    <span style='font-weight:bold;font-size:16px;font-family:Verdana;'>Telefone:</span> <span style='color:#000;font-size:14px;font-family:Verdana;'>{{ $telefone }}</span><br>
    <span style='font-weight:bold;font-size:16px;font-family:Verdana;'>Cargo:</span> <span style='color:#000;font-size:14px;font-family:Verdana;'>{{ $cargo }}</span><br>
    <span style='font-weight:bold;font-size:16px;font-family:Verdana;'>Mensagem:</span> <span style='color:#000;font-size:14px;font-family:Verdana;'>{{ $mensagem }}</span>
</body>
</html>
