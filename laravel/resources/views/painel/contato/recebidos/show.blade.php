@extends('painel.common.template')

@section('content')

    <legend>
        <h2>Contatos Recebidos</h2>
    </legend>

    <div class="form-group">
        <label>Data</label>
        <div class="well">{{ $contato->created_at }}</div>
    </div>

    <div class="row">
        <div class="col-md-6">
            <div class="form-group">
                <label>Nome do Condomínio</label>
                <div class="well">{{ $contato->nome_condominio }}</div>
            </div>

            <div class="form-group">
                <label>Endereço</label>
                <div class="well">{{ $contato->endereco }}</div>
            </div>

            <div class="form-group">
                <label>Quantidade de Apartamentos</label>
                <div class="well">{{ $contato->apartamentos }}</div>
            </div>

            <div class="form-group">
                <label>Funcionários</label>
                <div class="well">{{ $contato->funcionarios }}</div>
            </div>

            <div class="form-group">
                <label>Valor mensal do condomínio atual</label>
                <div class="well">{{ $contato->valor_mensal_condominio }}</div>
            </div>
        </div>
        <div class="col-md-6">
            <div class="form-group">
                <label>Nome</label>
                <div class="well">{{ $contato->nome }}</div>
            </div>

            <div class="form-group">
                <label>E-mail</label>
                <div class="well">
                    <button class="btn btn-info btn-sm clipboard" data-clipboard-text="{{ $contato->email }}" style="margin-right:5px;border:0;transition:background .3s">
                        <span class="glyphicon glyphicon-copy"></span>
                    </button>
                    {{ $contato->email }}
                </div>
            </div>

            <div class="form-group">
                <label>Telefone</label>
                <div class="well">{{ $contato->telefone }}</div>
            </div>

            <div class="form-group">
                <label>Cargo</label>
                <div class="well">{{ $contato->cargo }}</div>
            </div>

            <div class="form-group">
                <label>Mensagem</label>
                <div class="well">{{ $contato->mensagem }}</div>
            </div>
        </div>
    </div>

    <a href="{{ route('painel.contato.recebidos.index') }}" class="btn btn-default btn-voltar">Voltar</a>

@stop
