@include('painel.common.flash')

<div class="form-group">
    {!! Form::label('texto', 'Texto') !!}
    {!! Form::textarea('texto', null, ['class' => 'form-control ckeditor', 'data-editor' => 'textoLink']) !!}
</div>

<div class="form-group">
    {!! Form::label('missao', 'Missão') !!}
    {!! Form::textarea('missao', null, ['class' => 'form-control ckeditor', 'data-editor' => 'texto']) !!}
</div>

<div class="form-group">
    {!! Form::label('visao', 'Visão') !!}
    {!! Form::textarea('visao', null, ['class' => 'form-control ckeditor', 'data-editor' => 'texto']) !!}
</div>

<div class="form-group">
    {!! Form::label('valores', 'Valores') !!}
    {!! Form::textarea('valores', null, ['class' => 'form-control ckeditor', 'data-editor' => 'texto']) !!}
</div>

{!! Form::submit($submitText, ['class' => 'btn btn-success']) !!}
