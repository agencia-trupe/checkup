@include('painel.common.flash')

<div class="form-group">
    {!! Form::label('servicos', 'Serviços') !!}
    {!! Form::textarea('servicos', null, ['class' => 'form-control ckeditor', 'data-editor' => 'texto']) !!}
</div>

<div class="form-group">
    {!! Form::label('terceirizacao_beneficios', 'Terceirização Benefícios') !!}
    {!! Form::textarea('terceirizacao_beneficios', null, ['class' => 'form-control ckeditor', 'data-editor' => 'texto']) !!}
</div>

{!! Form::submit($submitText, ['class' => 'btn btn-success']) !!}
