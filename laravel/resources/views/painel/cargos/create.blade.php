@extends('painel.common.template')

@section('content')

    <legend>
        <h2><small>Cargos /</small> Adicionar Cargo</h2>
    </legend>

    {!! Form::open(['route' => 'painel.cargos.store', 'files' => true]) !!}

        @include('painel.cargos.form', ['submitText' => 'Inserir'])

    {!! Form::close() !!}

@endsection
