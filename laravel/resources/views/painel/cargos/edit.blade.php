@extends('painel.common.template')

@section('content')

    <legend>
        <h2><small>Cargos /</small> Editar Cargo</h2>
    </legend>

    {!! Form::model($registro, [
        'route'  => ['painel.cargos.update', $registro->id],
        'method' => 'patch',
        'files'  => true])
    !!}

    @include('painel.cargos.form', ['submitText' => 'Alterar'])

    {!! Form::close() !!}

@endsection
