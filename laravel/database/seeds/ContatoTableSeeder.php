<?php

use Illuminate\Database\Seeder;

class ContatoTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('contato')->insert([
            'email'           => 'contato@trupe.net',
            'fone'            => '11 3832.2031',
            'facebook'        => '#',
            'texto'           => 'Entre em contato hoje mesmo pelo telefone 11 3832.2031 ou pelo e-mail: <a href="mailto:rodolfo@gaboardiegomes.com.br">rodolfo@gaboardiegomes.com.br</a> e agende uma visita sem compromisso. Ficaremos felizes em poder colaborar. Atendemos toda grande São Paulo.',
            'endereco'        => 'Rua Barão de Jundiaí, 332 - Cjto. 04 - Cep: 05073-010 - Lapa - São Paulo SP<br>Tel: 11 3832.4284 - Tel/Fax: 11 3832.2031 - <a href="http://www.gaboardiegomes.com.br" target="_blank">www.gaboardiegomes.com.br</a>',
            'endereco_rodape' => 'Rua Barão de Jundiaí, 332 - Cjto. 04 - Cep: 05073-010 - Lapa - São Paulo SP - Tel: 11 3832.4284 - Tel/Fax: 11 3832.2031'
        ]);
    }
}
