<?php

use Illuminate\Database\Seeder;

class ConfiguracoesSeeder extends Seeder
{
    public function run()
    {
        DB::table('configuracoes')->insert([
            'nome_do_site'               => 'CHECK UP',
            'title'                      => 'CHECK UP &middot; serviços gerais',
            'description'                => '',
            'keywords'                   => '',
            'imagem_de_compartilhamento' => '',
            'analytics'                  => '',
        ]);
    }
}
